package sheridan;

public class Palindrome {
	
	public static boolean isPalindrome(String input) {
		if(input.isEmpty()) {
			return false;
		} 
		
		input = input.toLowerCase().replaceAll(" ", "");
		
		for(int i = 0, j = input.length() - 1; i < j; i++, j--) {
			if (input.charAt(i) != input.charAt(j)) {
				return false;
			}
		}
		
		return true;
	}
	
	
	public static void main(String[] args) {
		System.out.println("Is anna palindrome? " + Palindrome.isPalindrome("anna"));
		System.out.println("Is race car palindrome? " + Palindrome.isPalindrome("race car"));
		System.out.println("Is taco cat palindrome? " + Palindrome.isPalindrome("taco cat"));
		System.out.println("Is gabz palindrome? " + Palindrome.isPalindrome("gabz"));

	}

}
